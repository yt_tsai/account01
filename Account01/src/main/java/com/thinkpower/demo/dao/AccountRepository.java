package com.thinkpower.demo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.thinkpower.demo.entity.Account;

public interface AccountRepository extends JpaRepository<Account, Long> , AccountRepositoryCustom{



	public List<Account> findByName(String accountName);

}
