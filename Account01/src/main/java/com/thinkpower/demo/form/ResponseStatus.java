package com.thinkpower.demo.form;

import java.util.List;

import com.thinkpower.demo.entity.Account;

public class ResponseStatus {
	
	private String message;
	private List<Account> list;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<Account> getList() {
		return list;
	}

	public void setList(List<Account> list) {
		this.list = list;
	}

}
