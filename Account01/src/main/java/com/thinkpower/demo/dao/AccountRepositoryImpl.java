package com.thinkpower.demo.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


import org.hibernate.annotations.Where;
import org.springframework.beans.factory.annotation.Autowired;

import com.thinkpower.demo.entity.Account;


public class AccountRepositoryImpl implements AccountRepositoryCustom{
	
	@PersistenceContext
	public EntityManager em;
	
	@Override
	public int updateAccountByName(String name, String division, String email) {
		StringBuffer sql = new StringBuffer();
		// sql.append("SET SQL_SAFE_UPDATES=0;");
		sql.append("Update account02 ");
		sql.append("set division = :division, email = :email ");
		 sql.append("Where name like :name");
		
		Query query = em.createNativeQuery(sql.toString(), Account.class );
		
		query.setParameter("division", division);
		query.setParameter("email", email);
		query.setParameter("name", "%" + name +"%" );
		
		 int r = query.executeUpdate();
		 em.flush();
		 em.close();
		
		return r;
	}

	@Override
	public int deleteFindByFeild(String name) {
		StringBuffer sql = new StringBuffer();
		sql.append("DELETE FROM ");
		sql.append("account01.account02 ");
		sql.append("WHERE name =:name");
		Query query = em.createNativeQuery(sql.toString(), Account.class);
		query.setParameter("name", name);
		
		int delNumber = query.executeUpdate();
		em.flush();
		em.close();


		return delNumber;
	}
	
	@Override
	public List<Account> findByEmail(String email) {
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT * ");
		sql.append("FROM account02 a02 ");
		sql.append("WHERE a02.email Like :email");
		
		Query query = em.createNativeQuery(sql.toString(), Account.class);
		query.setParameter("email", "%" + email + "%");
		List<Account> emailList = (List<Account>) query.getResultList();
		
		return emailList;
	}

}