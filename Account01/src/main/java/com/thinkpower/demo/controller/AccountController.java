package com.thinkpower.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.thinkpower.demo.entity.Account;
import com.thinkpower.demo.form.AccountForm;
import com.thinkpower.demo.form.ResponseStatus;
import com.thinkpower.demo.service.AccountService;

@RestController
public class AccountController {
	
	@Autowired
	private AccountService accountService;
	
	@PostMapping(path = "/deleteAccount", consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE})
	public ResponseStatus deleteAccount(@RequestBody AccountForm accountForm) {
		Account account=accountService.deleteAccount(accountForm);
		ResponseStatus responseStatus=new ResponseStatus();
		
		if(account==null) {
			responseStatus.setMessage("刪除失敗!");
		}else {
			responseStatus.setMessage("刪除成功!");
		}
		responseStatus.setList(accountService.findAll());
		return responseStatus;
		
	}
	
	@PostMapping(path = "/addAccount", consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseStatus addAccount(@RequestBody AccountForm accountForm) {
		Account account = accountService.save(accountForm);
		ResponseStatus responseStatus = new ResponseStatus();
		if (account == null) {
			responseStatus.setMessage("新增失敗");
		} else {
			responseStatus.setMessage("新增成功");
		}
		responseStatus.setList(accountService.findAll());
		return responseStatus;
	}

	@GetMapping(path = "/getAllAccount") // 搜尋全部的Account
	public List<Account> getAllAccount() {
		List<Account> list = accountService.findAll();
		return list;
	}

		
	@PostMapping(path = "/updateAccount", consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseStatus updateAccount(@RequestBody AccountForm accountForm) {
		Account account = accountService.updateAccount(accountForm);
		ResponseStatus responseStatus = new ResponseStatus();
		if (account == null) {
			responseStatus.setMessage("更新失敗~~");
		} else {
			responseStatus.setMessage("更新成功");
		}
		responseStatus.setList(accountService.findAll());
		return responseStatus;
	}

	@Autowired
	private AccountService accountservice;
	
	@PostMapping(path = "/deleteAccountusingsql", consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseStatus deleteProduct(@RequestBody AccountForm accountForm) {
		
		ResponseStatus responseStatus = accountService.deleteFindByFeild(accountForm);
		
		responseStatus.setList(accountService.findAll());
		
		return responseStatus;
	}
	
	@PostMapping(path = "/updateAccountByName", consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseStatus updateAccountByName(@RequestBody AccountForm accountForm) {
		ResponseStatus responseStatus = accountService.updateAccountByName(accountForm);

		responseStatus.setList(accountService.findAll());
		return responseStatus;
	}

	@PostMapping(path = "/findByEmail", consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE}) 
	public ResponseStatus findByEmail(@RequestBody AccountForm accountForm) {
//		AccountService accountService = new AccountService();
		ResponseStatus responseStatus = new ResponseStatus();
		List<Account> list = accountService.findByEmail(accountForm);
		
		if (!list.isEmpty()) {
			responseStatus.setList(list); // 得第一筆資料
			responseStatus.setMessage("回傳符合條件之Email");
	}

	return responseStatus;
	}
}


