package com.thinkpower.demo.service;

import java.util.Optional;
import javax.transaction.Transactional;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.thinkpower.demo.dao.AccountRepository;
import com.thinkpower.demo.entity.Account;
import com.thinkpower.demo.form.AccountForm;
import com.thinkpower.demo.form.ResponseStatus;

@Transactional
@Service
public class AccountService {

	@Autowired
	private AccountRepository accountRepository;

	public List<Account> findAll() {
		return accountRepository.findAll();

	}

	public Account save(AccountForm accountform) {
		Account a = new Account();
		a.setName(accountform.getName());
		a.setEmail(accountform.getEmail());
		return accountRepository.save(a);
	}
	
	public Account updateAccount(AccountForm accountForm) {
		Optional<Account> data =  accountRepository.findById(accountForm.getId());
		
		Account account = null;
		if(data.isPresent()) {
			account = data.get();
			account.setName(accountForm.getName());
			account.setEmail(accountForm.getEmail());
			account.setDivision(accountForm.getDivision());
			accountRepository.save(account);			
		}		
		return account;
		
	}	


	public Account deleteAccount(AccountForm accountForm) {
		Optional<Account> data=accountRepository.findById(accountForm.getId());
		
		Account account=null;
		if (data.isPresent()) {
			account=data.get();
			
			account.setEmail(accountForm.getEmail());
			accountRepository.delete(account);
		}
		
		return account;
	}
	public ResponseStatus deleteFindByFeild(AccountForm accountForm){
		int list = accountRepository.deleteFindByFeild(accountForm.getName());
		
		ResponseStatus responseStatus = new ResponseStatus();

		if (list > 0) {
			responseStatus.setMessage("刪除成功");
		} else {
			responseStatus.setMessage("刪除失敗");
		}
		return responseStatus;
	}
	
	
	public ResponseStatus updateAccountByName(AccountForm accountForm) {
		int data = accountRepository.updateAccountByName(accountForm.getName(), accountForm.getDivision(), accountForm.getEmail());
		
		ResponseStatus responseStatus = new ResponseStatus();
		if(data > 0) {
				responseStatus.setMessage("更新成功");
			}else {
				responseStatus.setMessage("更新失敗");
			}
				
		return responseStatus;		
	}


	public List<Account> findByEmail (AccountForm accountForm) { // 依據Email搜尋
		List<Account> data=accountRepository.findByEmail(accountForm.getEmail());
		
		return data; 
	}

}
