package com.thinkpower.demo.dao;

import java.util.List;

import com.thinkpower.demo.entity.Account;

public interface AccountRepositoryCustom {
	
	public int updateAccountByName(String name, String division, String email);

	public int deleteFindByFeild(String name);
	
	public List<Account> findByEmail(String email); // 依據Email搜尋

}
